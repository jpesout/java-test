package com.etnetera.hr.data;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.Range;

/**
 * Simple data entity describing basic properties of every JavaScript framework.
 * 
 * @author Etnetera
 *
 */
@Entity
public class JavaScriptFramework {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(nullable = false, length = 30)
	@NotEmpty
    @Size(max = 30)
	private String name;
	
	@Column(nullable = true)
	private Date deprecationDate;
	
	@Column(nullable = true)
	@Range(min=1, max=100)
	private Integer hypeLevel;

	private Set<JavaScriptFrameworkVersion> versions;
	
	public JavaScriptFramework() {
	}

	public JavaScriptFramework(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDeprecationDate() {
		return deprecationDate;
	}
	
	public void setDeprecationDate(Date deprecationDate) {
		this.deprecationDate = deprecationDate;
	}
	
	public Integer getHypeLevel() {
		return hypeLevel;
	}
	
	public void setHypeLevel(Integer hypeLevel) {
		this.hypeLevel = hypeLevel;
	}
	
	@OneToMany(mappedBy = "javaScriptVersion", cascade = CascadeType.ALL)
    public Set<JavaScriptFrameworkVersion> getVersions() {
        return versions;
    }

    public void setVersions(Set<JavaScriptFrameworkVersion> versions) {
        this.versions = versions;
    }

	
	@Override
	public String toString() {
		return "JavaScriptFramework [id=" + id + 
				", name=" + name + 
				", deprecationDate = " + deprecationDate +
				", hypeLevel = " + hypeLevel + 
				", versions = " + versions.size() +
				"]";
	}

}
