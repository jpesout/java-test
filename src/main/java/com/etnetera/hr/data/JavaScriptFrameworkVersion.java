package com.etnetera.hr.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

/**
 * Simple data entity describing basic properties of JavaScript version.
 * 
 * @author Etnetera
 *
 */
@Entity
public class JavaScriptFrameworkVersion {

	@Id
	private Long id;

	@Column(nullable = false, length = 30)
	@NotEmpty
    @Size(max = 30)
	private String version;
	
	public JavaScriptFrameworkVersion() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	@Override
	public String toString() {
		return "JavaScriptFrameworkVersion [id=" + id + ", version=" + version + "]";
	}

}
